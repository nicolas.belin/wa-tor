import time
import random

PROPORTION_INITIALE_THON = 0.2
PROPORTION_INITIALE_REQUIN = 0.05
DUREE_GESTATION_THON = 2
DUREE_GESTATION_REQUIN = 4
ENERGIE_REQUIN = 3
MER = ()

###### FONCTIONS UTILITAIRES ########################

def contient_mer(carte, x, y):
    """
    Vrai ssi la cellule carte[y][x] ne contient pas de poisson.

    :param carte: (list) la carte des cellules
    :param x: (int) un indice, abscisse de la cellule
    :param y: (int) un indice, ordonnée de la cellule
    :return: (bool) Vrai si pas de poisson
    :CU: aucun
    """
    return len(carte[y][x]) == 0

def liste_cellules_voisines(carte, x, y, test):
    """
    Liste les cellules voisinnes de carte[y][x] qui satisfont la fonction test 
    
    :param carte: (list) la carte des cellules
    :param x: (int) un indice, abscisse de la cellule
    :param y: (int) un indice, ordonnée de la cellule
    :param test: (function) fonction booléenne testant une cellule
    :return: (list) la liste des cellules voisines qui satisfont le test
    :CU: aucun
    """
    hauteur = len(carte)
    longueur = len(carte[0])
    mody = lambda n: n % hauteur
    modx = lambda n: n % longueur
    liste = []
    for xp, yp in ((x, mody(y+1)), (modx(x+1), y), (x, mody(y-1)), (modx(x-1), y)):
        if test(carte, xp, yp):
            liste.append((xp, yp))
    return liste

def déplace_poisson(carte, x, y, cellules_voisines):
    """
    Déplace le poisson en carte[y][x] vers l'une des cellules_voisines, éventuellement un poisson naît en carte[y][x].
    :param carte: (list) la carte des cellules
    :param x: (int) un indice, abscisse de la cellule
    :param y: (int) un indice, ordonnée de la cellule
    :param cellules_voisines: (list) liste de cellules adjacentes
    :return: (NoneType) aucun
    :CU: modifie jusqu'à deux cellules de la carte
    """

    if contient_thon(carte, x, y):
        durée_gestation = DUREE_GESTATION_THON
        naissance_poisson = naissance_thon
    else:
        durée_gestation = DUREE_GESTATION_REQUIN
        naissance_poisson = naissance_requin
        
    poisson = carte[y][x]
    if poisson[0] == 0: # la gestation recommence
        poisson[0] = durée_gestation

    l = len(cellules_voisines)
    if l > 0: # le poisson se déplace
        nx, ny = cellules_voisines[random.randint(0, l-1)]
        carte[ny][nx] = poisson
        if poisson[0] == durée_gestation: # le poisson se reproduit
            naissance_poisson(carte, x, y)
        else:
            carte[y][x] = MER
        

###### THONS ######################################## 

def naissance_thon(carte, x, y):
    """
    Mettre un nouveau thon dans la cellule carte[y][x]

    :param carte: (list) la carte des cellules
    :param x: (int) un indice, abscisse de la cellule
    :param y: (int) un indice, ordonnée de la cellule
    :return: (NoneType) aucun
    :CU: modifie la cellule carte[y][x]
    """
    carte[y][x] = [DUREE_GESTATION_THON]

def contient_thon(carte, x, y):
    """
    Vrai ssi la cellule carte[y][x] contient un thon.

    :param carte: (list) la carte des cellules
    :param x: (int) un indice, abscisse de la cellule
    :param y: (int) un indice, ordonnée de la cellule
    :return: (bool) Vrai si présence d'un thon
    :CU: aucun
    """
    return len(carte[y][x]) == 1

def destin_thon(carte, x, y):
    """
    Destinée du thon présent dans la cellule carte[y][x].

    :param carte: (list) la carte des cellules
    :param x: (int) un indice, abscisse de la cellule
    :param y: (int) un indice, ordonnée de la cellule
    :return: (NoneType) auncun
    :CU: modifie jusqu'à deux cellules

    >>> carte = [[[3], ()], [[3], [1]]]
    >>> destin_thon(carte, 1, 1)
    >>> contient_thon(carte, 1, 1) and contient_thon(carte, 1, 0) and carte[0][1][0] == DUREE_GESTATION_THON
    True
    """

    carte[y][x][0] -= 1 # gestation
    voisines = liste_cellules_voisines(carte, x, y, contient_mer)
    déplace_poisson(carte, x, y, voisines)

    
###### REQUINS #####################################

def naissance_requin(carte, x, y):
    """
    Mettre un nouveau requin dans la cellule carte[y][x]

    :param carte: (list) la carte des cellules
    :param x: (int) un indice, abscisse de la cellule
    :param y: (int) un indice, ordonnée de la cellule
    :return: (NoneType) aucun
    :CU: modifie la cellule carte[y][x]
    """
    carte[y][x] = [DUREE_GESTATION_REQUIN, ENERGIE_REQUIN]

def contient_requin(carte, x, y):
    """
    Vrai ssi la cellule carte[y][x] contient un requin.

    :param carte: (list) la carte des cellules
    :param x: (int) un indice, abscisse de la cellule
    :param y: (int) un indice, ordonnée de la cellule
    :return: (bool) Vrai si présence d'un requin
    :CU: aucun
    """
    return len(carte[y][x]) == 2

def destin_requin(carte, x, y):
    """
    Destinée du requin présent dans la cellule carte[y][x].

    :param carte: (list) la carte des cellules
    :param x: (int) un indice, abscisse de la cellule
    :param y: (int) un indice, ordonnée de la cellule
    :return: (NoneType) auncun
    :CU: modifie jusqu'à deux cellules

    >>> carte = [[(), [2]], [(), [2, 2]]]
    >>> destin_requin(carte, 1, 1)
    >>> len(carte[1][1]) == 0 and len(carte[0][1]) == 2 and carte[0][1][1] == ENERGIE_REQUIN
    True
    """
    requin = carte[y][x]
    requin[1] -= 1 # énergie du requin
    if requin[1] == 0: # décès du requin
        carte[y][x] = MER
        return
    requin[0] -= 1 # gestation
    voisines_thon = liste_cellules_voisines(carte, x, y, contient_thon)
    l = len(voisines_thon)
    if l > 0: # le requin mange un thon
        voisines = voisines_thon
        requin[1] = ENERGIE_REQUIN # le requin est repu
    else:
        voisines = liste_cellules_voisines(carte, x, y, contient_mer)
    déplace_poisson(carte, x, y, voisines)


###### ÉVOLUTION DES POPULATIONS #########################
    
def peuple_cellule(carte, x, y):
    """
    Met éventuellement un poisson dans la cellule carte[y][x]

    :param carte: (list) la carte des cellules
    :param x: (int) un indice, abscisse de la cellule
    :param y: (int) un indice, ordonnée de la cellule
    :return: (NoneType) aucun
    :CU: modifie la cellule carte[y][x]
    """
    
    r = random.random()
    if r < PROPORTION_INITIALE_THON:
        naissance_thon(carte, x, y)
    elif r < PROPORTION_INITIALE_THON + PROPORTION_INITIALE_REQUIN:
        naissance_requin(carte, x, y)

def construit_carte_initiale(longueur, hauteur):
    """
    Construit de manière aléatoire la carte initiale.

    :param longueur: (int) nombre horizontal de cellules 
    :param hauteur: (int) nombre vertical de cellules 
    :return: (list) la carte, une liste a deux dimensions  
    :CU: aucune
    """
    carte = [[MER for _ in range(longueur)] for __ in range(hauteur)]
    for y in range(hauteur):
        for x in range(longueur):
            peuple_cellule(carte, x, y)
    return carte
            
def évolution_cellule(carte, x, y):
    """
    Fait evoluer la cellule carte[y][x]
    
    :param carte: (list) la carte des cellules
    :param x: (int) un indice, abscisse de la cellule
    :param y: (int) un indice, ordonnée de la cellule
    :return: (NoneType) aucun
    :CU: modifie jusqu'à deux cellules de la carte
    """
    if contient_thon(carte, x, y):
        destin_thon(carte, x, y)
    elif contient_requin(carte, x, y):
        destin_requin(carte, x, y)

def évolution_nombreuses_cellules(carte):
    """
    Fait evoluer longueur*hauteur cellules de la carte
    
    :param carte: (list) la carte des cellules
    :return: (NoneType) aucun
    :CU: modifie de nombreuses cellules de la carte
    """
    hauteur = len(carte)
    longueur = len(carte[0])
    for _ in range(longueur * hauteur):
        évolution_cellule(carte,
                          random.randint(0, longueur - 1),
                          random.randint(0, hauteur - 1))
           
if __name__ == '__main__':
    from interface import Display_matrix

    def prépare_affichage(carte):
        """
        Transforme la carte pour l'afficher avec Display_Matrix

        :param carte: (list) la carte des cellules
        :return: (list) une matrice d'entiers représentant la carte
        :CU: aucun
        """
        carte_scalaire = []
        hauteur = len(carte)
        longueur = len(carte[0])
        for y in range(hauteur):
            ligne_scalaire = []
            for x in range(longueur):
                ligne_scalaire.append(len(carte[y][x]))
            carte_scalaire.append(ligne_scalaire)
        return carte_scalaire

    def main():
        """
        La fonction principale, où commence l'exécution.
        """
        
        LONGUEUR = 60 # Longueur de la grille
        HAUTEUR = 40  # Hauteur de la grille
        W = 600       # Longueur de la fenêtre
        H = 400       # Hauteur de la fenêtre
        DT = 0.5      # intervalle de temps entre deux itérations (en s)
        
        random.seed()
        carte = construit_carte_initiale(LONGUEUR, HAUTEUR)
        couleurs = (0x000000, 0x0000FF, 0x00FF00)
        dm = Display_matrix(prépare_affichage(carte), W, H, couleurs, 'Wa-Tor')

        boucle = True
        while boucle:
            time.sleep(DT)
            évolution_nombreuses_cellules(carte)
            dm.update_matrix(prépare_affichage(carte))
            boucle = dm.check_event()

        dm.quit()

    main()
