"""   28/5/2019

Touches :
q : quitter
f : plein écran
"""
import pygame
from math import floor

class Interface:
    """
    Ouvre une fenêtre de dimension w*h, avec pour titre 'caption'

    Touches actives:
    'q' : quitter
    'f' : plein écran
    """

    def __init__(self, w, h, caption):
        self.width = w
        self.height = h
        pygame.display.init()
        self.screen = pygame.display.set_mode((w, h), pygame.RESIZABLE)
        pygame.display.set_caption(caption)
        pygame.mouse.set_visible(False)
        self.looping = True
        self.is_fullscreen = False
        self.keys_dict = dict()
        self.user_events_dict = dict()
        self.drawing_funcs = []
        self.register_key('q', self.stop)
        self.register_key('f', self.toggle_fullscreen)

    def stop(self):
        self.post(pygame.QUIT)

    def quit(self):
        pygame.quit()
        
    def post(self, type, *args):
        pygame.event.post(pygame.event.Event(type, *args))

    def post_user(self, name, args):
        pygame.event.post(pygame.event.Event(pygame.USEREVENT, utype = name, args = args))

    def refresh(self):
        for f in self.drawing_funcs:
            f(self.screen)
        pygame.display.flip()
        
    def toggle_fullscreen(self):
        if self.is_fullscreen:
            self.is_fullscreen = False
            self.screen = pygame.display.set_mode((self.w_old,self.h_old), pygame.RESIZABLE)
        else:
            self.is_fullscreen = True
            self.w_old, self.h_old = self.screen.get_size()
            self.screen = pygame.display.set_mode(pygame.display.list_modes(0,pygame.FULLSCREEN)[0], pygame.FULLSCREEN)
        self.refresh()
            
    def register_key(self, unicode, func):
        self.keys_dict[unicode] = func

    def register_user_event(self, name, func):
        self.user_events_dict[name] = func

    def register_drawing(self, func):
        self.drawing_funcs.append(func)

    def set_timer(self, func, time):
        self.timer_func = func
        pygame.time.set_timer(pygame.USEREVENT+1, time)

    def check_event(self, ev = None):
        if not ev:
            ev = pygame.event.poll()
        if ev.type == pygame.QUIT:
            self.looping = False
        elif ev.type == pygame.KEYDOWN:
            if ev.unicode in self.keys_dict:
                self.keys_dict[ev.unicode]()
        elif ev.type == pygame.VIDEORESIZE:
            self.screen = pygame.display.set_mode(ev.size, pygame.RESIZABLE)
            self.refresh()
        elif ev.type == pygame.USEREVENT:
            if ev.utype in self.user_events_dict:
                self.user_events_dict[ev.utype](*ev.args)
        elif ev.type == pygame.USEREVENT+1:
            self.timer_func()
        return self.looping
        
    def loop(self):
        while self.looping:
            ev = pygame.event.wait()
            self.check_event(ev)
        self.quit()

class Display_matrix(Interface):
    """
    Affiche la matrice d'indices 'matrix' dans une fenêtre
    en utilisant la liste de couleurs 'colormap' et le titre 'caption'.
    """

    def __init__(self, matrix, width, height, colormap, caption = 'Display_matrix'):
        """
        :param matrix: (list) une matrice d'indices dans la liste 'colormap' 
        :param width: (int) largeur de la fenêtre
        :param height: (int) hauteur de la fenêtre
        :param colormap: (list) liste de couleurs au format RGB 0xRRGGBB
        :param caption: (str) le titre de la fenêtre
        :return: (NoneType) aucun
        :CU: ouvre une fenêtre et affiche la matrice
        """
        
        super().__init__(width, height, caption)
        self.matrix = matrix
        self.colormap = colormap
        self.w = len(matrix[0])
        self.h = len(matrix)
        self.register_drawing(self.display)
        self.refresh()
        
    def display(self, surface):
        x = lambda i: floor(i * ws / self.w)
        y = lambda j: floor(j * hs / self.h)
        ws, hs = surface.get_size()
        for j in range(self.h):
            for i in range(self.w):
                color = self.colormap[self.matrix[j][i]]
                rect = (x(i), y(j), x(i+1)-x(i)-1, y(j+1)-y(j)-1)  
                surface.fill(color, rect)

    def update_matrix(self, matrix):
        """
        Change la matrice à utiliser.
        :param matrix: (list) la nouvelle matrice à utiliser
        """
        self.matrix = matrix
        self.refresh()
                
if __name__ == "__main__":

    matrix = [[(x+y) % 2 for x in range(60)] for y in range(40)]
    colormap = (0x0000FF, 0x000444)
    md = Display_matrix(matrix, 600, 400, colormap, 'Test matrice')

    md.loop()
