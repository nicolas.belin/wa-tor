# WA-TOR

**Simulation proie-prédateur**

## I. La marche aléatoire des thons

Le thon nait, se déplace et se reproduit en mer.

### A. La mer torique
La mer est représentée par la liste à deux dimensions `carte`. Une cellule de `carte` est "vide" quand elle contient `()`. Les poissons seront représentés par des listes de 1 et 2 éléments.

La mer est torique dans la mesure où la ligne juste "au-dessus" de la ligne la plus haute est la ligne du bas, la colonne "à droite" de la colonne la plus à droite est la colonne de gauche, etc...

1. Définir une fonction `construit_carte_initiale(longueur, hauteur)` qui initialise `carte` en utilisant les variables globales constantes suivantes:
   * `LONGUEUR = 6`
   * `HAUTEUR = 4`
   * `MER = ()`

2. Définir une fonction `prépare_affichage(carte)` qui transforme `carte` en une liste de même dimensions contenant les longueurs de ses éléments.

3. Tester la fonction précédente avec :

    ```
    from interface import Display_matrix

    carte = construit_carte_initiale(LONGUEUR, HAUTEUR)
    x, y = 2, 1
    carte[y][x] = [2] # un thon
        
    couleurs = (0x000000, 0x0000FF, 0x00FF00)
    W = 600       # Longueur de la fenêtre
    H = 400       # Hauteur de la fenêtre
    dm = Display_matrix(prépare_affichage(carte), W, H, couleurs, 'Wa-Tor')
    ```

### B. Déplacement du thon
Un thon se déplace de manière aléatoire dans les quatres directions : haut, bas, gauche, droite.

1. Définir une fonction `itération_thon(carte, x, y)` qui déplace le thon `[2]` dans `carte[y][x]` vers l'une des cellules voisines, et renvoit la nouvelle position du poisson.

2. Observer les évolutions du thon précédent en rajoutant :

    ```
    import time
    import random
        
    DT = 0.2      # intervalle de temps entre deux itérations (en s)
    boucle = True
    while boucle:
        time.sleep(DT)
        x, y = itération_thon(carte, x, y) 
        dm.update_matrix(prépare_affichage(carte))
        boucle = dm.check_event()

    dm.quit()
    ```

### C. Vie et reproduction des thons

Les thons ont un temps de gestation de 2. À chaque itération, ce temps est décrémenté. Quand il atteint 0 le thon se reproduit, en laissant un nouveau thon dans la cellule qu'il vient de quitter. Après la reproduction, la gestation recommence. Le thon ne se reproduit pas s'il ne peut pas se déplacer.

1. **Naissance** : Modifier la fonction `construit_carte_initiale()` pour que
20 % des cellules contiennent un thon, c'est à dire `[2]`.

2. **Déplacement** : Modifier le programme pour choisir aléatoirement une série de `LONGUEUR*HAUTEUR` cellules et tester la présence d'un thon. Le cas échéant, le faire se déplacer.

3. **Reproduction** : Implémenter la reproduction des thons dans la fonction `itération_thon()`.


## II. L'ennemi héréditaire

+ Les requins sont représentés à leur naissance par une paire `[4, 3]`.

+ Le nombre 4 est le temps de gestation du requin. La reproduction des requins est similaire à celle des thons par ailleurs.

+ Le nombre 3 est l'énergie du requin. Cette énergie est décrémenté à chaque itération. Quand l'énergie atteint 0, le requin meurt.

+ Si un thon est présent sur l'une des quatres cellules voisines d'un requin, celui-ci mange le thon et son énergie revient à 3.

*Implémenter les requins et leurs comportements dans le programme précédent.*
